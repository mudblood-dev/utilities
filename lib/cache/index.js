const LRUCache = require('./lru-cache');
const LRUServer = require('./lru-server');

module.exports = class Cache {
  constructor(cacheType, options) {
    switch (cacheType) {
      case 'MEMORY': {
        this.cache = new LRUCache(options?.capacity || 100);
        break;
      }
      case 'LRU_SERVER': {
        const urlRegex =
          /^(https?|HTTPS?):\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)$/;
        if (!urlRegex.test(options.serverAddress)) {
          throw new Error(
            `Server address ${options.serverAddress} is not a valid URL.`
          );
        }
        this.cache = new LRUServer(options.serverAddress);
      }
      default: {
        throw new Error(`Cache type ${cacheType} is not supported.`);
      }
    }
  }

  get(key) {
    return this.cache.get(key);
  }
  set(key, value, expiresIn) {
    return this.cache.set(key, value, expiresIn);
  }
  del(key) {
    return this.cache.del(key);
  }

  async fetch(key, fetchDataCallback, expiresIn) {
    const cachedData = this.get(key);
    if (cachedData !== null) {
      return cachedData;
    }

    const fetchedData = await fetchDataCallback();
    this.set(key, fetchedData, expiresIn);
    return fetchedData;
  }
};
