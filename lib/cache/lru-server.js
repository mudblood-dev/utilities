const axios = require('axios');

module.exports = class LRUServer {
  constructor(serverAddress) {
    this.serverAddress = serverAddress;
  }

  async set(key, value) {
    try {
      const response = await axios.post(`${this.serverAddress}/cache/${key}`, {
        data: value,
      });
      return response.data;
    } catch (error) {
      throw new Error(`Error setting cache: ${error.message}`);
    }
  }

  async get(key) {
    try {
      const response = await axios.get(`${this.serverAddress}/cache/${key}`);
      return response.data;
    } catch (error) {
      throw new Error(`Error getting cache: ${error.message}`);
    }
  }

  async del(key) {
    try {
      const response = await axios.delete(`${this.serverAddress}/cache/${key}`);
      return response.data;
    } catch (error) {
      throw new Error(`Error deleting cache: ${error.message}`);
    }
  }

  async getAll() {
    try {
      const response = await axios.get(`${this.serverAddress}/cache`);
      return response.data;
    } catch (error) {
      throw new Error(`Error getting all cache entries: ${error.message}`);
    }
  }
}