module.exports = class LRUCache {
  constructor(capacity) {
    this.capacity = capacity;
    this.cache = new Map();
    this.head = { key: null, value: null, next: null, prev: null };
    this.tail = { key: null, value: null, next: null, prev: null };
    this.head.next = this.tail;
    this.tail.prev = this.head;
  }

  moveToHead(node) {
    this.removeNode(node);
    node.next = this.head.next;
    node.prev = this.head;
    this.head.next.prev = node;
    this.head.next = node;
  }

  removeNode(node) {
    node.prev.next = node.next;
    node.next.prev = node.prev;
  }

  get(key) {
    if (this.cache.has(key)) {
      const node = this.cache.get(key);
      this.moveToHead(node);
      return node.value;
    }
    return null;
  }

  del(key) {
    if (this.cache.has(key)) {
      const node = this.cache.get(key);
      this.removeNode(node);
      this.cache.delete(key);
    }
  }

  set(key, value) {
    if (this.cache.has(key)) {
      const node = this.cache.get(key);
      node.value = value;
      this.moveToHead(node);
    } else {
      if (this.cache.size >= this.capacity) {
        // Remove the least recently used item (tail)
        const tailKey = this.tail.prev.key;
        this.cache.delete(tailKey);
        this.removeNode(this.tail.prev);
      }

      // Add the new item to the cache and list
      const newNode = { key, value, next: null, prev: null };
      this.cache.set(key, newNode);
      this.moveToHead(newNode);
    }
  }
};
