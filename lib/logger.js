const winston = require('winston');

const { Console, File } = winston.transports;
const { combine, timestamp, prettyPrint, json } = winston.format;

module.exports.createLogger = function createLogger(loggingOptions) {
  let transports = [];
  for (let type in loggingOptions) {
    if (loggingOptions[type].enabled) {
      if (type == 'console') {
        transports.push(new Console(loggingOptions[type]));
      }
      if (type == 'file') {
        if (process.env.LOG_FILE_PATH) {
          transports.push(
            new winston.transports.File({ filename: process.env.LOG_FILE_PATH })
          );
        } else {
          throw new Error('Please provide LOG_FILE_PATH environment variable');
        }
      }
    }
  }
  let logger = winston.createLogger({
    exitOnError: false,
    transports: transports,
    format: combine(
      winston.format.errors({ stack: true }),
      timestamp(),
      process.env.NODE_ENV && process.env.NODE_ENV !== 'development'
        ? json()
        : prettyPrint()
    ),
  });
  return logger;
};
