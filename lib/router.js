module.exports.Router = class Router {
  #routes;
  #allRoutesExposed;
  constructor(basePath = '', authController) {
    this.basePath = basePath;
    this.authController = authController;
    this.#routes = {
      get: new Map(),
      post: new Map(),
      put: new Map(),
      delete: new Map(),
      patch: new Map(),
    };
    this.#allRoutesExposed = false;
  }
  getAllRegisteredRoutes() {
    // we have to limit this function to expose all routes only once.
    if (this.#allRoutesExposed) return;
    this.#allRoutesExposed = true;
    return this.#routes;
  }
  addAuthController(authController) {
    this.authController = authController;
  }
  addChildRouter(childRouter) {
    if (typeof this.authController === 'function') {
      childRouter.addAuthController(this.authController);
    }
    const childRouterRoutes = childRouter.getAllRegisteredRoutes();
    if (!childRouterRoutes) return;
    for (const method of Object.keys(childRouterRoutes)) {
      for (const [childRoutePath, childRouteHandler] of childRouterRoutes[
        method
      ].entries()) {
        this.#routes[method].set(
          `${this.basePath}${childRoutePath}`,
          childRouteHandler
        );
      }
    }
  }
  get({ path, controller, auth, middlewares, omitBasePath = false }) {
    this.#routes.get.set(
      `${omitBasePath ? '/' : this.basePath}${path}`,
      this.#createRouteHandler({
        controller,
        auth,
        middlewares,
      })
    );
  }
  post({ path, controller, auth, middlewares, omitBasePath = false }) {
    this.#routes.post.set(
      `${omitBasePath ? '/' : this.basePath}${path}`,
      this.#createRouteHandler({
        controller,
        auth,
        middlewares,
      })
    );
  }
  put({ path, controller, auth, middlewares, omitBasePath = false }) {
    this.#routes.put.set(
      `${omitBasePath ? '/' : this.basePath}${path}`,
      this.#createRouteHandler({
        controller,
        auth,
        middlewares,
      })
    );
  }
  delete({ path, controller, auth, middlewares, omitBasePath = false }) {
    this.#routes.delete.set(
      `${omitBasePath ? '/' : this.basePath}${path}`,
      this.#createRouteHandler({
        controller,
        auth,
        middlewares,
      })
    );
  }
  patch({ path, controller, auth, middlewares, omitBasePath = false }) {
    this.#routes.patch.set(
      `${omitBasePath ? '/' : this.basePath}${path}`,
      this.#createRouteHandler({
        controller,
        auth,
        middlewares,
      })
    );
  }
  #createRouteHandler({ controller, auth, middlewares }) {
    return async (req) => {
      try {
        if (typeof this.authController === 'function' && auth !== false) {
          const result = await this.authController(req);
          if (!result.isSuccess) {
            return {
              statusCode: result.statusCode || 401,
              body: {
                status: 'error',
                ...(result.customErrorCode
                  ? { customErrorCode: result.customErrorCode }
                  : {}),
                ...(result.message ? { message: result.message } : {}),
              },
              ...(typeof result.headers === 'object'
                ? { headers: result.headers }
                : {}),
              ...(Array.isArray(result.cookies)
                ? { cookies: result.cookies }
                : {}),
            };
          }
          if (typeof result.authData === 'object')
            req.authData = result.authData;
        }
        if (Array.isArray(middlewares)) {
          for (const middleware of middlewares) {
            const result = middleware(req);
            if (result && result instanceof Promise) {
              await result;
            }
          }
        }
        const result = await controller(req);
        const statusCode = result.statusCode || 200;
        let body = result.rawData ?? {
          status: result.status || 'success',
          ...(result.data ? { data: result.data } : {}),
          ...(result.customErrorCode
            ? { customErrorCode: result.customErrorCode }
            : {}),
          ...(result.message ? { message: result.message } : {}),
        };
        return {
          statusCode,
          body,
          ...(typeof result.headers === 'object'
            ? { headers: result.headers }
            : {}),
          ...(Array.isArray(result.cookies) ? { cookies: result.cookies } : {}),
        };
      } catch (err) {
        req.logger.error(err);
        return {
          statusCode: 500,
          body: {
            status: 'error',
            message: 'Something went wrong',
          },
        };
      }
    };
  }
};
