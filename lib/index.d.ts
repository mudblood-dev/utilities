import { IncomingMessage } from 'http';
import * as pgLib from 'pg';
import winstonLib, { Logger } from 'winston';
import * as bb from 'busboy';

type PoolConfig = Pick<
  pgLib.PoolConfig,
  'host' | 'database' | 'password' | 'port' | 'ssl' | 'connectionString'
>;

export interface CockroachDbConfig extends PoolConfig {
  username?: string;
  poolSize?: number;
  isSSL?: boolean;
  sslCaCert?: string;
  sslCertKey?: string;
  sslCert?: string;
}

export function deepFreeze<T>(obj: T): Readonly<T>;

export enum CacheTypes {
  LRU_SERVER = 'LRU_SERVER',
  MEMORY = 'MEMORY',
}
export class Cache {
  constructor(cacheType: CacheTypes, options: Record<string, any>);
  get<T>(key: string): Promise<T>;
  set<T>(key: string, value: T, expiresIn: number): Promise<void>;
  del(key: string): Promise<void>;
  fetch<T>(key: string, fetchCallback: () => T, expiresIn: number): Promise<T>;
}

export class CockroachDbUtil {
  constructor(config: CockroachDbConfig);
  getCockroachLib(): typeof pgLib;
  getConnection(args?: { database?: string }): Promise<pgLib.PoolClient>;
  executeQueryUsingExistingConnection<R extends pgLib.QueryResultRow>(
    args: Pick<pgLib.QueryConfig<any[]>, 'text' | 'values'>
  ): Promise<pgLib.QueryResult<R>>;
  executeQuery<R extends pgLib.QueryResultRow>(args: {
    query: string;
    values?: any[];
    database?: string;
  }): Promise<pgLib.QueryResult<R>>;
}
export { DatabaseError as CockroachDBError } from 'pg';

interface BaseTransportConfig {
  enabled: boolean;
}

export interface LoggingConfig {
  console: BaseTransportConfig & winstonLib.transports.ConsoleTransportOptions;
  file?: BaseTransportConfig & winstonLib.transports.FileTransportOptions;
}

export { Logger } from 'winston';

export function createLogger(config: LoggingConfig): Logger;

export interface RouteControllerResponse {
  statusCode?: number;
  status?: 'success' | 'error';
  customErrorCode?: string;
  message?: string;
  headers?: Record<string, string>;
  data?: Record<string, any>;
  rawData?: any;
  cookies?: Array<CookieOptions>;
}
export type routeController = (
  req: RequestObject
) => RouteControllerResponse | Promise<RouteControllerResponse>;

export type AuthControllerResponse = Omit<
  RouteControllerResponse,
  'data' | 'rawData' | 'status'
> & { isSuccess: boolean; authData?: Record<string, any> };
export type authController = (
  req: RequestObject
) => AuthControllerResponse | Promise<AuthControllerResponse>;

export interface RequestObject {
  params: Record<string, any>;
  body?: Record<string, any>;
  query: Record<string, any>;
  headers: Record<string, any>;
  cookies: Record<string, any>;
  authData?: Record<string, any>;
  logger: Logger;
  info: {
    method: method;
    url: string;
    ip: string;
    host: string;
  };
  rawHTTPRequest: IncomingMessage & { busboy: bb.Busboy };
}
export type routeMiddleware = (req: RequestObject) => void | Promise<void>;
export interface CookieOptions {
  key: string;
  value: string;
  options?: {
    httpOnly?: boolean;
    secure?: boolean;
    domain?: string | null;
    path?: string | null;
    expiresAt?: Date | null;
    maxAge: number | null;
    sameSite?: false | 'Strict' | 'None' | 'Lax';
  };
}
export type routeHandler = (req: RequestObject) => Promise<{
  statusCode: number;
  body: any;
  headers?: Record<string, string>;
  cookies?: Array<CookieOptions>;
}>;
export interface RouteConfig {
  path: string;
  controller: routeController;
  auth?: boolean;
  middlewares?: Array<routeMiddleware>;
}
export type method = 'get' | 'post' | 'put' | 'delete' | 'patch';
export interface Route {
  method: method;
  path: string;
  handler: routeHandler;
}
export class Router {
  constructor(basePath: string, authController?: authController);
  basePath: string;
  authController: routeController;
  private routes: {
    get: Map<string, routeHandler>;
    post: Map<string, routeHandler>;
    put: Map<string, routeHandler>;
    delete: Map<string, routeHandler>;
    patch: Map<string, routeHandler>;
  };
  get(config: RouteConfig): void;
  post(config: RouteConfig): void;
  put(config: RouteConfig): void;
  delete(config: RouteConfig): void;
  patch(config: RouteConfig): void;
  getAllRegisteredRoutes(): Array<Route>;
  addChildRouter(childRouter: Router): void;
  addAuthController(authController: authController): void;
}
