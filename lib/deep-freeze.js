module.exports.deepFreeze = function deepFreeze(obj) {
  if (typeof obj !== 'object' || obj === null) {
    return obj;
  }

  // Get all property names of the object
  const propNames = Object.getOwnPropertyNames(obj);

  // Freeze the object itself
  Object.freeze(obj);

  // Recursively freeze properties that are objects or arrays
  for (const propName of propNames) {
    const prop = obj[propName];

    if (typeof prop === 'object' && prop !== null) {
      deepFreeze(prop);
    }
  }

  return obj;
};
