const { Pool: PgPool } = require('pg');
const fs = require('fs');

class CockroachDbUtil {
  defaultDatabase
  constructor({
    host,
    connectionString,
    port,
    username,
    password,
    database,
    poolSize,
    isSSL,
    sslCaCert,
    sslCertKey,
    sslCert,
  }) {
    let ssl;
    if (isSSL) {
      ssl = {
        rejectUnauthorized: false,
      };
      if (sslCaCert) {
        ssl.ca = fs.readFileSync(sslCaCert).toString();
      } else {
        ssl.ca = fs.readFileSync('/cockroach-certs/ca.crt').toString();
      }

      if (sslCert) {
        ssl.cert = fs.readFileSync(sslCert).toString();
      } else {
        ssl.cert = fs
          .readFileSync('/cockroach-certs/client.root.crt')
          .toString();
      }

      if (sslCertKey) {
        ssl.key = fs.readFileSync(sslCertKey).toString();
      } else {
        ssl.key = fs
          .readFileSync('/cockroach-certs/client.root.key')
          .toString();
      }
    }
    this.defaultDatabase = database;
    this.cockroachPool = new PgPool({
      connectionString,
      user: username,
      host: host,
      database: database,
      password: password,
      port: port,
      max: poolSize,
      ssl: ssl,
    });
  }

  getCockroachLib() {
    return require('pg');
  }

  async getConnection({ database }) {
    const connection = await this.cockroachPool.connect();
    await connection.query(`USE ${database ? database : this.defaultDatabase}`);
    return connection;
  }

  async executeQueryUsingExistingConnection({
    connection,
    query,
    values,
    releaseConnectionAfterQuery,
  }) {
    try {
      return await connection.query({
        text: query,
        values: values ? values : [],
      });
    } catch (e) {
      throw e;
    } finally {
      if (releaseConnectionAfterQuery && connection) {
        await connection.release();
      }
    }
  }

  async executeQuery({ database, query, values }) {
    let connection;
    try {
      connection = await this.getConnection({ database });
      return await connection.query({
        text: query,
        values: values ? values : [],
      });
    } catch (e) {
      throw e;
    } finally {
      if (connection) {
        await connection.release();
      }
    }
  }
}

module.exports.CockroachDbUtil = CockroachDbUtil;
